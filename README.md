# Sawmill

A Java library for generating Java source code. Generates readable, visually beautiful code. Ideal for 
generating API stubs, POJOs or other boilerplate code.
